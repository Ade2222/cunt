public class Main {

    public static void main(String args[]){
        //Variable zum Überpruefen, ob alle Tests durchlaufen
        boolean passed = true;

        //erstellen von Testvariablen
        int[] testvector1 = {1,2,3};
        int[] testvector2 = {3,2,1};
        int[] testvector3 = {1,1,1};
        int[] testvector4 = {3,1,3};
        int[] testvector5 = {1,0,0};
        int[] testvector6 = {0,1,0};

        //Testen des Skalarproduktes
        int skpr1 = skalarProdukt(testvector1, testvector2);
        int skpr2 = skalarProdukt(testvector4, testvector3);
        if(skpr1 == 10 && skpr2 == 7) {
            System.out.println("Methode für das Skalarprodukt liefert richtiges Ergebnis");
        }else{
            System.out.println("Methode für das Skalarprodukt liefert falsches Ergebnis");
            passed = false;
        }

        //Testen des Kreuzproduktes
        int[] krpr1 = kreuzProdukt(testvector5, testvector6);
        int[] krpr2 = kreuzProdukt(testvector1, testvector2);
        if(krpr1[0] == 0 && krpr1[1] == 0 && krpr1[2] == 1
                && krpr2[0] == -4 && krpr2[1] == 8 && krpr2[2] == -4){
            System.out.println("Methode für Kreuzprodukt liefert richtiges Ergebnis");
        }else{
            System.out.println("Methode für das Kreuzprodukt liefert ein falsches Ergebnis");
            passed = false;
        }

        // finales Ergebniss, ob alle Tests durchlaufen
        if (passed)
            System.out.println("\n" + "Alle Tests bestanden");
    }


    /**Die Methode soll das Skalarprodukt von zwei Vektoren berechnen und dieses dann zurückgeben.
     *
     * @param a Vektor zur Berechnung des Kreuzproduktes als Array der Laenge 3
     * @param b Vektor zur Berechnung des Kreuzproduktes als Array der Laenge 3
     * @return gibt das Skalarprodukt der beiden übergebenen Vektoren ans int zurück
     */

    public static int skalarProdukt(int[] a, int[] b){
        int result = 0;
        //Das Program kann mich mal




        return result;
    }


    /**Die Methode soll das Kreuzprodukt der beiden uebergebenen Vektoren berechnen und das Ergebnis dann zurückgeben.
     *
     * @param a Vektor zur Berechnung des Kreuzproduktes als Array der Laenge 3
     * @param b Vektor zur Berechnung des Kreuzproduktes als Array der Laenge 3
     * @return gibt das Kreuzprodukt der beiden übergebenen Vektoren zurück als Array der Länge 3
     */
    public static int[] kreuzProdukt(int[] a, int[] b){
        int[] result = new int[3];
        //Hier bitte eigenen Code einfügen

        

        return result;
    }
}
